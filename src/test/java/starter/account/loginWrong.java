package starter.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class loginWrong {
    @Managed(driver = "firefox")
    WebDriver driver;
    @Steps
    LoginStepsWrongPass loginStepsWrongPass;

    @Given("I open linkedin pagee")
    public void i_open_linkedin_page() {
        // Write code here that turns the phrase above into concrete actions
        loginStepsWrongPass.OpenLinkedinPage();
    }

    @When("I input email and passwordd")
    public void i_input_email_and_password() {
        // Write code here that turns the phrase above into concrete actions
        loginStepsWrongPass.InputEmailAndPassword();
    }

    @Then("I click login and see message \"Wrong password\"")
    public void i_click_login() {
        // Write code here that turns the phrase above into concrete actions
        loginStepsWrongPass.ClickButtonLogin();
    }

//    @Then("I see message \"Wrong password\" ")
//    public void verification_page(){
//        loginStepsWrongPass.verification_page();
//    }

}
