package starter.account;

import net.thucydides.core.steps.ScenarioSteps;
import starter.pages.account.LinkedinLogin;
import starter.pages.account.LinkedinLoginSuccess;


public class LoginStepsSuccess extends ScenarioSteps {
    LinkedinLoginSuccess linkedinLogin;

    public void OpenLinkedinPage() {
        linkedinLogin.open();
    }

    public void InputEmailAndPassword(){
        linkedinLogin.InputEmail();
        linkedinLogin.InputPassword();
    }

    public void ClickButtonLogin(){
        linkedinLogin.ClickButtonLogin();
    }

    public void verification_page(){
        linkedinLogin.getTitle();
    }

}

//brew install chromedriver