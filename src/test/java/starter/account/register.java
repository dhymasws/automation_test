package starter.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class register {
    @Managed(driver = "firefox")
    WebDriver driver;
    @Steps
    RegisterSteps registerSteps;

    @Given("I open linkedin register page")
    public void i_open_linkedin_page() {
        // Write code here that turns the phrase above into concrete actions
        registerSteps.OpenLinkedinPeoplePage();
    }
    @When("I input email and password for register")
    public void i_input_fn_and_ln() {
        // Write code here that turns the phrase above into concrete actions
        registerSteps.InputFirstAndLastName();
    }
    @When("I click agree&join button")
    public void i_click_search() {
        // Write code here that turns the phrase above into concrete actions
        registerSteps.ClickButtonSearch();
    }
    @Then("I see next step register page linkedin")
    public void people_page(){
        registerSteps.verification_page();
    }

}