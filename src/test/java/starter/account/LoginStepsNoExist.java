package starter.account;

import starter.pages.account.LinkedinLogin;
import starter.pages.account.UserNoExist;

public class LoginStepsNoExist {
    LinkedinLogin linkedinLogin;
    UserNoExist userNoExist;

    public void OpenLinkedinPage() {
        linkedinLogin.open();
    }

    public void InputEmailAndPassword(){
        userNoExist.InputEmail();
        userNoExist.InputPassword();
    }

    public void ClickButtonLogin(){
        linkedinLogin.ClickButtonLogin();
    }

    public void verification_page(){
        linkedinLogin.getTitle();
    }
}
