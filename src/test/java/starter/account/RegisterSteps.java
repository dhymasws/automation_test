package starter.account;

import net.thucydides.core.steps.ScenarioSteps;
import starter.pages.account.LinkedinRegister;

public class RegisterSteps extends ScenarioSteps {
    LinkedinRegister linkedinRegister;

    public void OpenLinkedinPeoplePage() {
        linkedinRegister.open();
    }
    public void InputFirstAndLastName(){
        linkedinRegister.input_fn();
        linkedinRegister.input_ln();
    }

    public void ClickButtonSearch(){
        linkedinRegister.click_search();
    }

    public void verification_page(){
        linkedinRegister.getTitle();
    }

}
