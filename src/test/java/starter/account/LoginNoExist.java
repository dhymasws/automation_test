package starter.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class LoginNoExist {
    @Managed(driver = "firefox")
    WebDriver driver;
    @Steps
    LoginStepsNoExist loginSteps;

    @Given("I open linkedin page login")
    public void i_open_linkedin_page_login() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.OpenLinkedinPage();
    }

    @When("I input email and password login")
    public void i_input_email_and_password_linkedin() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.InputEmailAndPassword();
    }

    @Then("I click login and see error message user not found displayed")
    public void i_click_login_linkedin() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.ClickButtonLogin();
    }

}
