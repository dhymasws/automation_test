package starter.account;

import net.thucydides.core.steps.ScenarioSteps;
import starter.pages.account.LinkedinLogin;
import starter.pages.account.LinkedinLoginWrong;


public class LoginStepsWrongPass extends ScenarioSteps {
    LinkedinLoginWrong linkedinLoginWrong;

    public void OpenLinkedinPage() {
        linkedinLoginWrong.open();
    }

    public void InputEmailAndPassword(){
        linkedinLoginWrong.InputEmail();
        linkedinLoginWrong.InputPassword();
    }

    public void ClickButtonLogin(){
        linkedinLoginWrong.ClickButtonLogin();
    }

}

//brew install chromedriver