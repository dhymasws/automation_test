package starter.account;

import net.thucydides.core.steps.ScenarioSteps;
import starter.pages.account.LinkedinLogin;
import static org.junit.Assert.assertTrue;


public class LoginSteps extends ScenarioSteps {
    LinkedinLogin linkedinLogin;

    public void OpenLinkedinPage() {
        linkedinLogin.open();
    }

    public void InputEmailAndPassword(){
        linkedinLogin.InputEmail();
        linkedinLogin.InputPassword();
    }

    public void ClickButtonLogin(){
        linkedinLogin.ClickButtonLogin();
    }

    public void verification_page(){
        linkedinLogin.getTitle();
    }

}

//brew install chromedriver