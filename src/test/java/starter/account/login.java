package starter.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class login {
    @Managed(driver = "firefox")
    WebDriver driver;
    @Steps
    LoginSteps loginSteps;

    @Given("I open linkedin page")
    public void i_open_linkedin_page() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.OpenLinkedinPage();
    }

    @When("I input email and password")
    public void i_input_email_and_password() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.InputEmailAndPassword();
    }

    @When("I click login")
    public void i_click_login() {
        // Write code here that turns the phrase above into concrete actions
        loginSteps.ClickButtonLogin();
    }

    @Then("I see verification page")
    public void verification_page(){
        loginSteps.verification_page();
    }

}
