package starter.pages.account;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.linkedin.com")
public class LinkedinLogin extends PageObject {

    @FindBy(xpath="//input[@id='session_key']")
    WebElement fieldEmail;
    public void InputEmail(){
        fieldEmail.sendKeys("email@gmail.com");
    }

    @FindBy(xpath="//input[@id='session_password']")
    WebElement fieldPassword;
    public void InputPassword(){
        fieldPassword.sendKeys("password123");
    }

    @FindBy(xpath="//*[@id=\"main-content\"]/section[1]/div/div/form/button")
    WebElement buttonLogin;
    public void ClickButtonLogin(){
        buttonLogin.click();
    }
}
